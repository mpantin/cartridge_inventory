from django.db import models
from django.contrib import admin
from django.utils import timezone
from datetime import datetime, timedelta
from django import forms
import os


def getTime():
    today = timezone.now()
    return today


link = '/var/projects/cartridge_inventory/cartridge_inventory/raw_data/'
consHPColor = ['Black', 'Cyan', 'Yellow', 'Magenta',
               'Image Transfer', 'Image Fuser']
consXerMono = ['Black', 'Drum']
Oki0s = ['B430', 'B460', 'B470']  # Models of OKI B430 etc.
Oki1s = ['B431', 'B461', 'B471']  # Models of OKI B431 etc.
direction = (("In", "Received"), ("Out", "Sent"))


def createCartridgeResidueName(a='black'):
    """ This function creates cartridges names if str 'full' is on entrance
     it will return var. name with full set of colors. """
    if a == 'full':
        name = [s.lower() + 'Residue' for s in consHPColor[:4]]
    else:
        name = ['blackResidue']
    return name


def createDrumResidueName(a='black'):
    """ This function creates drum cartridges names if str 'full' is on entrance
    it will return var. name with full set of colors. """
    if a == 'full':
        name = [s.lower() + 'DrumResidue' for s in consHPColor[:4]]
    else:
        name = ['blackDrumResidue']
    return name


def getAllPrinters():
    values = Printer.objects.all().values_list('dataFile', flat=True)
    return values


class Parser:
    """ This class is created for processing files.
    All functions that are working with raw files or data are here """

    def __getData(self, printerData, path=link):
        """ Function for getting data from files """
        text = open(path + printerData).read()
        return text

    def __snmpFileParser(self, printerData, path=link):
        """ This function is used for parsing data from
         files which are created by SNMP request \ response """
        fileData = self.__getData(printerData, path)
        d = fileData.rstrip().split('\n')
        status = [data.split(' ')[3] for data in d]
        status[1:] = [int(s) for s in status[1:]]
        return status

    def __a3a4Separator(self, totalPages):
        """This function is used for separate A4 an A3 paper consumption. Printers doesn`t return by SNMP amount of
        A4 and A3 printed pages, only total amount.
        But analysing data from printers it was discovered that approximately 1/4 of total printed pages - A3 format"""
        a3a4 = ['a3counter', 'a4counter']
        pages = []
        pages.append(totalPages / 4)  # A3 pages
        pages.append(totalPages - (totalPages / 4))  # A4 pages
        info = dict(zip(a3a4, pages))
        return info

    def __hpSmartParser(self, status, dividers):
        """ This function decides how to prepare data for different HP printers. Return dictionary"""
        fileData = ['ip', 'totalPages']
        status[2] = status[2] / dividers[0]
        info = {"a4counter": status[1]}
        if len(status) <=3:
            cartridges = createCartridgeResidueName()
            fileData.extend(cartridges)
        elif len(status) == 8:
            cartridges = [(consumable.lower() + 'Residue').split(' ')[-1] for consumable in consHPColor]
            status[3:6] = [s / dividers[1] for s in status[3:6]]
            status[6] = status[6] / dividers[2]
            status[7] = status[7] / dividers[3]
            fileData.extend(cartridges)
            a3a4pages = self.__a3a4Separator(status[1])
            info.update(a3a4pages)
        info.update(dict(zip(fileData, status)))
        return info

    def __xeroxSmartParser(self, status, dividers):
        """ This function decides how to prepare data for different Xerox printers. Returns dictionary """
        fileData = ['ip', 'totalPages']
        info = {"a4counter": status[1]}
        if len(status) <= 3:
            status[2] = status[2] / dividers[0]
            cartridges = createCartridgeResidueName()
            fileData.extend(cartridges)
        elif len(status) == 6:
            status[2] = status[2] / dividers[0]
            status[3:] = [s / dividers[1] for s in status[3:]]
            cartridges = createCartridgeResidueName('full')
            fileData.extend(cartridges)
        else:
            status[3] = int(status[3] / dividers[0])
            fileData.append('wasteBoxStatus')
            drums = ''
            if len(status) == 5:
                status[4] = int(status[4] / dividers[1])
                cartridges = createCartridgeResidueName()
                drums = createDrumResidueName()
            elif len(status) == 8:
                status[4:7] = [s / dividers[1] for s in status[4:7]]
                status[7] = status[7] / dividers[2]
                cartridges = createCartridgeResidueName('full')
                drums = createDrumResidueName()
            else:
                status[4:7] = [s / dividers[1] for s in status[4:7]]
                status[7] = status[7] / dividers[2]
                status[8:] = [s / dividers[3] for s in status[8:]]
                cartridges = createCartridgeResidueName('full')
                drums = createDrumResidueName('full')
            fileData.extend(cartridges)
            fileData.extend(drums)
            a3a4pages = self.__a3a4Separator(status[1])
            info.update(a3a4pages)
        info.update(dict(zip(fileData, status)))  # Dictionary for inserting in database
        return info

    def __okiParser(self, status, dividers):
        """ This function decides how to prepare data for different OKI printers. Returns dictionary """
        fileData = ['ip','totalPages']
        info = {"a4counter": status[1]}
        if status[3] <= 0:
            status[3] = 0
        else:
            status[3] = status[3] / dividers[0]
        cartridges = createCartridgeResidueName()
        drums = createDrumResidueName()
        fileData.extend(cartridges)
        fileData.extend(drums)
        info.update(dict(zip(fileData, status)))
        return info

    def __Xerox(self, printerData, status):
        """ This function chooses dividing values for different Xerox printers and
        calls smart parser to prepare data. Returns dictionary """
        dividers = [1]
        if all(s in printerData for s in ['xer', '5230']):
            dividers = [65, 1020]
        elif any(s in printerData for s in ['7232', '7242']):
            dividers = [41, 14, 419]
        elif all(s in printerData for s in ['xer', '7225']):
            dividers = [46, 29, 1, 1]
        elif all(s in printerData for s in ['xer', '7120']):
            dividers = [46, 29, 672, 513]
        elif all(s in printerData for s in ['xer', '5325']):
            dividers = [68, 1020]
        elif all(s in printerData for s in ['xer', '3300']):
            dividers = [80]
        elif all(s in printerData for s in ['xer', 'pe120']):
            dividers = [-1134]  # Xerox PE120 shows only Full or empty status. Full is 3400, emty - 0. Our system is marking -3 Value as OK (It was taken from wastebox status: -3 == OK, -2 == No wastebox, 0 == Full )
        elif all(s in printerData for s in ['xer', '6180']):
            dividers = [80, 60]
        info = self.__xeroxSmartParser(status, dividers)
        return info

    def __Oki(self, printerData, status):
        """ This function choses dividing values for different
        OKI printers and calls OKI smart parser. Returns dictionary"""
        dividers = [1]
        if any(s in printerData for s in Oki0s):  # parse oki B430\470 etc.
            dividers = [250]
        elif any(s in printerData for s in Oki1s):
            dividers = [300]
        info = self.__okiParser(status, dividers)
        return info

    def __HP(self, printerData, status):
        """ This function choses dividing values for different
        HP printers and calls OKI smart parser. Returns dictionary"""
        dividers = [1]
        if all(s in printerData for s in ['hp', '5550']):
            dividers = [130, 120, 1200, 1500]
        info = self.__hpSmartParser(status, dividers)
        return info

    def consStatus(self, printerData, path=link):
        """Function is made to get consumable status, takes
        file name and consumables list. Returns dict with consumable status"""
        values = {}
        status = self.__snmpFileParser(printerData, path)
        if 'hp' in printerData:
            values = self.__HP(printerData, status)
        elif 'xer' in printerData:
            values = self.__Xerox(printerData, status)
        elif 'oki' in printerData:
            values = self.__Oki(printerData, status)
        return values


class Files(models.Model):
    """ This class is used to gether filenames parse them prepare data and update the database"""
    name = models.CharField(max_length=100)

    def __uincode__(self):
        return self.name

    def __getListOfFiles(self, path=link):
        """Private function is getting the names of files from dir.
        Also This function checking if the file have got any content"""
        raw_files = list(os.listdir(path))
        files = [f for f in raw_files
                 if os.stat(path + f).st_size > 0]
        return files

    def __parseFileName(self, printerData):
        """This function is dividing file name into pieces combines into dict. and returns it"""
        parts = printerData.split('_')
        preName = '%s %s' % (parts[2], parts[3])
        preLocation = '%s %s' % (parts[0], parts[1])
        data = {'name': preName, 'location': preLocation}
        return data

    def renameDataFile(self, oldFile, newFile):
        """This function is created to rename DataFile name in existing entries.
        In my case, this function is called from ./cartridge_inventory/views.py
        It takes 2 arguments for old and new names."""
        objects = GraphBase.objects.filter(dataFile=oldFile).values()
        for obj in objects:
            obj['dataFile'] = newFile
            GraphBase.objects.filter(id=obj["id"]).update(**obj)
        objects = ChangesCounter.objects.filter(dataFile=oldFile).values()
        for obj in objects:
            obj['dataFile'] = newFile
            ChangesCounter.objects.filter(id=obj["id"]).update(**obj)
        return 0

    def __dataConstruction(self, fileName, counters):
        """ Prepares data from file name and counters.
        Returns appropriate list that can be used in create or update functions.Inserts date and time of operation"""
        today = getTime()
        data = self.__parseFileName(fileName)
        data.update({'dataFile': fileName})
        data.update({'lastUpdate': today})
        data.update(counters)
        return data

    def __keyMaker(self, printerData):
        """This is to make appropriate keys for dict. from list of consumables ex.:'"blackResidue'"""
        val = list()
        # if all(s in printerData for s in ['5550', 'cons']):
        if all(s in printerData for s in ['5550', 'hp']):
            val = [c.lower().split()[-1] + 'Residue' for c in consHPColor]
        elif all(s in printerData for s in ['xer', 'colorA4']):
            val = createCartridgeResidueName('full')
        elif all(s in printerData for s in ['xer', 'monoA4']):
            val = createCartridgeResidueName()
        elif all(s in printerData for s in ['xer', 'mono']):
            val = createCartridgeResidueName()
            drumName = createDrumResidueName()
            val.append('wasteBoxStatus')
            val.extend(drumName)
        elif any(s in printerData for s in ['7232', '7242']):
            val = createCartridgeResidueName('full')
            drumName = createDrumResidueName()
            val.append('wasteBoxStatus')
            val.extend(drumName)
        elif all(s in printerData for s in ['xer', 'color']):
            val = createCartridgeResidueName('full')
            drumName = createDrumResidueName('full')
            val.append('wasteBoxStatus')
            val.extend(drumName)
        elif all(s in printerData for s in ['oki', 'mono']):
            val = createCartridgeResidueName()
            drumName = createDrumResidueName()
            val.extend(drumName)
        return val

    def __getOldCounters(self, val, printFile):
        """Returns a dict with old consumable status"""
        valDict = dict()
        for v in val:
            if all(s in printFile for s in ['5550', 'hp']):
                updater = HPColorA3.objects.filter(dataFile=printFile).values(v)
                valDict.update(updater[0])
            elif all(s in printFile for s in ['xer', 'monoA4']):
                updater = XeroxMonoA4.objects.filter(dataFile=printFile).values(v)
                valDict.update(updater[0])
            elif all(s in printFile for s in ['xer', 'colorA4']):
                updater = XeroxColorA4.objects.filter(dataFile=printFile).values(v)
                valDict.update(updater[0])
            elif all(s in printFile for s in ['xer', 'mono']):
                updater = XeroxMonoA3.objects.filter(dataFile=printFile).values(v)
                valDict.update(updater[0])
            elif any(s in printFile for s in ['7232', '7242']):
                updater = XeroxColorA3Old.objects.filter(dataFile=printFile).values(v)
                valDict.update(updater[0])
            elif all(s in printFile for s in ['xer', 'color']):
                updater = XeroxColorA3New.objects.filter(dataFile=printFile).values(v)
                valDict.update(updater[0])
            elif all(s in printFile for s in ['oki', 'mono']):
                updater = OkiMonoA4.objects.filter(dataFile=printFile).values(v)
                valDict.update(updater[0])
        return valDict

    def __dataLog(self, printerFile, printerData, consumables):
        """This function logs all statuses every 1800 seconds from printers for data analyzes"""
        today = getTime()
        objects = GraphBase.objects.all().values_list('dataFile', flat=True)
        if printerFile in objects:
            obj = GraphBase.objects.filter(dataFile=printerFile).order_by('-date')
            latest_dict = obj.values('date')[0]  # Returns dict with time
            latest = latest_dict.get('date')
            delta = today - latest  # Returns timedelta
            if delta.seconds >= 1800:
                for cons in consumables:
                    val_list = obj.filter(consumable=cons).values('counter')[0]
                    if val_list.get('counter') != printerData[cons]:  # This condition is made to log if only status was changed.
                        GraphBase.objects.create(date=today, dataFile=printerFile, consumable=cons,
                                                 counter=printerData[cons])
        else:
            for cons in consumables:
                GraphBase.objects.create(date=today, dataFile=printerFile, consumable=cons, counter=printerData[cons])
        return 0

    def __deleteRecordIfExpired(self, printerData):
        """This function will automatically delete printer from the base if it was not updated for 1 day,
        but data is coming from it"""
        today = getTime()
        check = 1
        printerFile = printerData['dataFile']
        printer = Printer.objects.get(dataFile=printerFile)
        lastUpdate = printer.lastUpdate
        delta = today - lastUpdate
        if delta.days >= 1:
            Printer.objects.get(dataFile=printerFile).delete()
            check = 0
        return check

    def __changeMark(self, val, printFile, oldValDict, printerData):
        """This function is making a record to database when the cartridge is changed in the printer"""
        today = getTime()
        for c in val:
            if c == "wasteBoxStatus":
                if (int(oldValDict[c]) < int(printerData[c])) and (
                    (int(printerData[c]) - int(oldValDict[c]) >= 90) and
                        (int(printerData[c]) - int(oldValDict[c]) <= 100)):  # (printerData[c]-oldValDict[c]>30) - this to avoid situations when previous status was 0 and next status is 1 or 2 or 5 (could be when the cartridge was shaken)
                    ChangesCounter.objects.create(date=today, dataFile=printFile, consumable=c[:-6], counter=1)  # If the replacement occurred - put the mark in database.
                elif int(oldValDict[c]) == 0 and int(printerData[c]) == -3:  # Handles wastebox replacement
                    ChangesCounter.objects.create(date=today, dataFile=printFile, consumable=c[:-6], counter=1)  # If the replacement occurred - put the mark in database.
            elif (int(oldValDict[c]) < int(printerData[c])) and ((int(printerData[c]) - int(oldValDict[c]) >= 85) and (
                    int(printerData[c]) - int(oldValDict[c]) <= 100)):  # (printerData[c]-oldValDict[c]>30) - this to avoid situations when previous status was 0 and next status is 1 or 2 or 5 (could be when the cartridge was shaken)
                ChangesCounter.objects.create(date=today, dataFile=printFile, consumable=c[:-7],
                                              counter=1)  # If the replacement occurred - put the mark in database.
        return 0

    def __valuesCheck(self, val, oldValDict, printerData):
        """ This function must be used to avoid entry of small \ big or 0 \ 100 values due to some troubles with printer
         page representation \ wrong snmp status.
        Example: the cartridge status was 80%. Error occurred, returned status is 0. After printer reboot status is 80%.
        The condition for checking consists of 3 steps. There can be 3 situations with consumables:
        1) old data >= new data. Correct. This is a normal situation while consuming resources old == 100%, new == 99% => old >= new
        2) old data <= new data. Correct. Cartridge replacement occurred. old == 1%, new == 99% => old <= new
        3) old data > new data. InCorrect. When problem with data from a printer occurred you can get old == 90%, new == 0% => old >= new.
        But the next status for new will be new == 90% and old == 0% which our system will recognise as cartridge change. It is vital to handle this.
        4) old data < new data. Incorrect. Also problems with status but with opposite value of new data. old == 60%, new == 100% => old >= new
        So the solution is simple. We should exclude the InCorrect situations and include both Correct."""
        check = 0
        for c in val:
            if int(oldValDict[c]) >= int(printerData[c]) and ((int(oldValDict[c]) - int(printerData[c])) <= 15):  # Condition 1
            # if int(oldValDict[c]) >= int(printerData[c]) and ((int(oldValDict[c])-int(printerData[c])) <= 50):
                check = 1
            elif int(oldValDict[c]) <= 10 and 95 <= int(printerData[c]) <= 100:  # Condition 2
            # elif int(oldValDict[c]) < 90 and int(printerData[c]) >= 30 and int(printerData[c]) <= 100: # Condition 2
                check = 1
            elif int(oldValDict[c]) < int(printerData[c]) and (int(printerData[c]) - int(oldValDict[c])) <= 10:  # This condition is made to allow fluctuations between old and new data in 5% range
                check = 1
            else:
                check = 0
                break
        return check

    # TO MAKE FUNCTIONAL DECOMPOSITION OF FUNCTION __updatePrinterCounters

    def __updatePrinterCounters(self, printerData):
        """Takes printerData as appropriate dict to import in database. And updates a model"""
        printFile = printerData['dataFile']  # get printer file name - unique identifier
        pages = Paper()
        # oldValDict = dict()
        val = self.__keyMaker(printFile)
        oldValDict = self.__getOldCounters(val, printFile)
        check = self.__valuesCheck(val, oldValDict, printerData)
        if check:  # Check for changes and update models if everything is alright with data
            self.__changeMark(val, printFile, oldValDict, printerData)
            val.append('totalPages')  # We will log a paper also
            self.__dataLog(printFile, printerData, val)
            pagesLoad = pages.printedPages(printFile)
            printerData.update(pagesLoad)
            if all(s in printFile for s in ['5550', 'hp']):
                HPColorA3.objects.filter(dataFile=printFile).update(**printerData)
            elif all(s in printFile for s in ['xer', 'monoA4']):
                XeroxMonoA4.objects.filter(dataFile=printFile).update(**printerData)
            elif all(s in printFile for s in ['xer', 'colorA4']):
                XeroxColorA4.objects.filter(dataFile=printFile).update(**printerData)
            elif all(s in printFile for s in ['xer', 'mono']):
                XeroxMonoA3.objects.filter(dataFile=printFile).update(**printerData)
            elif any(s in printFile for s in ['7232', '7242']):
                XeroxColorA3Old.objects.filter(dataFile=printFile).update(**printerData)
            elif all(s in printFile for s in ['xer', 'color']):
                XeroxColorA3New.objects.filter(dataFile=printFile).update(**printerData)
            elif all(s in printFile for s in ['oki', 'mono']):
                OkiMonoA4.objects.filter(dataFile=printFile).update(**printerData)
        return 0

    def __addNewPrinter(self, printerData):
        """Takes printerData as appropriate dict to import in database. And creates a model"""
        # printerData.update({'ip': '0.0.0.0'})
        printFile = printerData['dataFile']
        if all(s in printFile for s in ['5550', 'hp']):
            HPColorA3.objects.create(**printerData)
        elif all(s in printFile for s in ['xer', 'monoA4']):
            XeroxMonoA4.objects.create(**printerData)
        elif all(s in printFile for s in ['xer', 'colorA4']):
            XeroxColorA4.objects.create(**printerData)
        elif all(s in printFile for s in ['xer', 'mono']):
            XeroxMonoA3.objects.create(**printerData)
        elif any(s in printFile for s in ['7232', '7242']):
            XeroxColorA3Old.objects.create(**printerData)
        elif all(s in printFile for s in ['xer', 'color']):
            XeroxColorA3New.objects.create(**printerData)
        elif all(s in printFile for s in ['oki', 'mono']):
            OkiMonoA4.objects.create(**printerData)
        return 0

    def lookUp(self, path=link):
        """ Look up through files and make decision: create or update model in database"""
        data = Parser()
        files = self.__getListOfFiles(path)
        objects = getAllPrinters()
        for f in files:
            try:  # Handle situation when there is wrong data in the file or content from printer does not contain useful information
                counters = data.consStatus(f, path)
                printerData = self.__dataConstruction(f, counters)
                if f in objects:
                    check = self.__deleteRecordIfExpired(printerData) # Checking if data is expired in DB but received from printer.
                    if check:
                        self.__updatePrinterCounters(printerData)
                else:
                    self.__addNewPrinter(printerData)
            except IndexError:
                pass

class Paper:
    """
    This class is created to make analyses of paper consumination etc.
    """
    
    def __timeDelta(self, days):
        today = getTime()
        delta = today - timedelta(days)
        return delta

    def __getTotalPages(self):
        """Get Returns list of int values"""
        values = Printer.objects.all().values_list('totalPages', flat=True)
        values = [int(obj) for obj in values]
        return values

    def totalPagesConsumed(self):
        total = sum(self.__getTotalPages())
        return total

# To be removed from here
    def pagesByPrinterThisDay(self, printerFile):
        today = getTime()
        yesterday = self.__timeDelta(28)
        values_list = GraphBase.objects.filter(consumable='totalPages', dataFile=printerFile, date__lte=today, date__gte=yesterday).values()
        values_list = [int(c.get('counter')) for c in values_list]
        total = sum(values_list)
        return total

    def pagesByPrinterThisMonth(self, printerFile):
        today = getTime()
        monthAgo = self.__timeDelta(28)
        values_list = GraphBase.objects.filter(consumable='totalPages', dataFile=printerFile, date__lte=today, date__gte=monthAgo).values()
        values_list = [int(c) for c in values_list]
        total = sum(values_list)
        return total

# To here
    def __pagesByPrinter(self, printerFile, days):
        today = getTime()
        deltaDays = self.__timeDelta(days)
        values_list = GraphBase.objects.filter(consumable='totalPages', dataFile=printerFile, date__lte=today, date__gte=deltaDays).values()  # Returns dict
        # values_list.sort()  # sorting the list to get time-accurate values. Pages counter can only be increased
        # values_list = [int(c.get('counter')) - int(values_list[0]) for c in values_list]  # We are minusing first value to get real amount of printed pages
        values_list = [int(c.get('counter')) for c in values_list]  # Get value from dict and create a new list
        values_list.sort()
        # val_list = [c - values_list[0] for c in values_list]  # Findout exact value of printed pages
        total = values_list[-1] - values_list[0]
        return total

    def printedPages(self, printerFile):
        d = [1, 7, 30, 365]
        key = ['printedToday', 'printedThisWeek', 'printedThisMonth', 'printedThisYear']
        val = [self.__pagesByPrinter(printerFile, c) for c in d]
        values = dict(zip(key, val))
        return values

    def __mostLoadedPrinterWeekly(self):
        pass

class Kind(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Color(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class Consumable(models.Model):
    kind = models.ForeignKey(Kind)
    color = models.ForeignKey(Color)


class Printer(models.Model):
    name = models.CharField(max_length=50)
    location = models.CharField(max_length=50)
    ip = models.IPAddressField()
    consumables = models.ManyToManyField(Consumable, blank=True)
    dataFile = models.CharField(max_length=100)
    inventory_number = models.CharField(max_length=40, blank=True, null=True)
    lastUpdate = models.DateTimeField()
    totalPages = models.IntegerField()
    printedToday = models.IntegerField(blank=True, null=True)
    printedThisWeek = models.IntegerField(blank=True, null=True)
    printedThisMonth = models.IntegerField(blank=True, null=True)
    printedThisYear = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name']


class A4Printer(Printer):
    a4counter = models.IntegerField()


class A3Printer(A4Printer):
    a3counter = models.IntegerField()


class HPColorA3(A3Printer):
    blackResidue = models.IntegerField(blank=True, null=True)
    cyanResidue = models.IntegerField(blank=True, null=True)
    yellowResidue = models.IntegerField(blank=True, null=True)
    magentaResidue = models.IntegerField(blank=True, null=True)
    transferResidue = models.IntegerField(blank=True, null=True)  # Transfer Kit status Residue
    fuserResidue = models.IntegerField(blank=True, null=True)  # Fuser Kit status Residue


class XeroxColorA3Old(A3Printer):
    blackResidue = models.IntegerField(blank=True, null=True)
    cyanResidue = models.IntegerField(blank=True, null=True)
    yellowResidue = models.IntegerField(blank=True, null=True)
    magentaResidue = models.IntegerField(blank=True, null=True)
    blackDrumResidue = models.IntegerField(blank=True, null=True)
    wasteBoxStatus = models.IntegerField(blank=True, null=True)


class XeroxColorA3New(A3Printer):
    blackResidue = models.IntegerField(blank=True, null=True)
    cyanResidue = models.IntegerField(blank=True, null=True)
    yellowResidue = models.IntegerField(blank=True, null=True)
    magentaResidue = models.IntegerField(blank=True, null=True)
    blackDrumResidue = models.IntegerField(blank=True, null=True)
    cyanDrumResidue = models.IntegerField(blank=True, null=True)
    yellowDrumResidue = models.IntegerField(blank=True, null=True)
    magentaDrumResidue = models.IntegerField(blank=True, null=True)
    wasteBoxStatus = models.IntegerField(blank=True, null=True)


class XeroxMonoA3(A3Printer):
    blackResidue = models.IntegerField(blank=True, null=True)
    blackDrumResidue = models.IntegerField(blank=True, null=True)
    wasteBoxStatus = models.IntegerField(blank=True, null=True)


class XeroxMonoA4(A4Printer):
    blackResidue = models.IntegerField(blank=True, null=True)


class XeroxColorA4(A4Printer):
    blackResidue = models.IntegerField(blank=True, null=True)
    cyanResidue = models.IntegerField(blank=True, null=True)
    yellowResidue = models.IntegerField(blank=True, null=True)
    magentaResidue = models.IntegerField(blank=True, null=True)


class OkiMonoA4(A4Printer):
    blackResidue = models.IntegerField(blank=True, null=True)
    blackDrumResidue = models.IntegerField(blank=True, null=True)


class ChangesCounter(models.Model):
    date = models.DateTimeField()
    dataFile = models.CharField(max_length=100)  # Is used to identify printer
    consumable = models.CharField(max_length=100)
    counter = models.IntegerField()


class GraphBase(models.Model):
    date = models.DateTimeField()
    dataFile = models.CharField(max_length=100)
    consumable = models.CharField(max_length=100)
    counter = models.IntegerField()


class Cartridge(models.Model):
    model = models.CharField(max_length=100)
    color = models.CharField(max_length=100, default="Black")


class EmptyCartridgeInventory(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    cartridge = models.ForeignKey(Cartridge)
    amount = models.PositiveSmallIntegerField()
    direction = models.CharField(choices=direction, max_length=100)

    @classmethod
    def saver(self, diction):
        if diction:
            self.objects.create(**diction)
        return 0

    @classmethod
    def dict_validator(self, diction):
        if diction:  # Check if diction not empty
            model_fields = self._meta.get_all_field_names()
            diction_keys = list(diction)
            if set(diction_keys).issubset(set(model_fields)):  # checking if our dict keys is exact wat we expect.
                if isinstance(diction['cartridge'], Cartridge):  # Separates form input and raw_json
                    self.saver(diction)
        return 0

    @classmethod
    def count_difference(self):
        """
        This function counts how much cartridges of each types were sent and received from service center
        and the difference, to be aware how much cartridges we should receive.
        """
        cartridges = Cartridge.objects.all()
        container = list()
        info = dict()
        for cartridge in cartridges:
            try:
                sent_all = EmptyCartridgeInventory.objects.filter(direction='Out', cartridge=cartridge).order_by('-date')
                received_all = EmptyCartridgeInventory.objects.filter(direction='In', cartridge=cartridge).order_by('-date')
                sent = sum([s.amount for s in sent_all])
                received = sum([s.amount for s in received_all])
                last_sent = sent_all[0].amount
                last_received = received_all[0].amount
                diff = sent - received  # counts total difference
                cartridge_name = cartridge.model + " " + cartridge.color
                info.update({"cartridge_name": cartridge_name, "last_sent": last_sent, "last_received": last_received, "total_diff": diff})
                container.append(info)
            except IndexError:
                pass
        return container


class CustomCartridgeTypeChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.model + " " + obj.color


class CustomCartridgeColorChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.color


class EmptyCartridgeInventoryForm(forms.Form):
    """ This class defines form which will be used for inserting of cartridges which are sent for refilling.
        EmptyCartridgeInventory class is used to store this info. """
    cartridge = CustomCartridgeTypeChoiceField(queryset=Cartridge.objects.all())
    amount = forms.IntegerField(required=True,min_value=0, max_value=100)
    direction = forms.ChoiceField(choices=direction)

