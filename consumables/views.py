from django.shortcuts import render
from itertools import chain
from django.http import HttpResponse
from django.db.models import Q
from datetime import datetime
from models import Printer, HPColorA3, XeroxColorA3New, XeroxColorA3Old, XeroxMonoA3, ChangesCounter, OkiMonoA4, \
    XeroxMonoA4, XeroxColorA4, EmptyCartridgeInventory, EmptyCartridgeInventoryForm

# cons=['name', 'lastUpdate', 'location', 'blackResidue', 'cyanResidue', 'yellowResidue', 'magentaResidue', 'transferResidue', 'fuserResidue', 'dataFile']

models = [HPColorA3, XeroxColorA3New, XeroxColorA3Old, XeroxMonoA3, OkiMonoA4, XeroxMonoA4, XeroxColorA4]


def get_counters(p):
    """This function is made to return changes for the group of printers which can be
    identified by dataFile. Ex. p = 5550 - is all HP 5550"""
    counters = ChangesCounter.objects.filter(dataFile__icontains=p).order_by('dataFile')
    return counters


def show_HPColor(request):
    HPprinters = HPColorA3.objects.values('name', 'inventory_number', 'lastUpdate', 'location', 'blackResidue',
                                          'cyanResidue', 'yellowResidue', 'ip', 'pk',
                                          'magentaResidue', 'transferResidue', 'fuserResidue', 'dataFile')
    counters = get_counters("5550")
    title = "HP Color printers"
    return render(request, 'short_printer_report.html', {'printers': HPprinters, 'counters': counters, 'title': title})


def show_XeroxColorA3(request):
    NewModels = XeroxColorA3New.objects.values('name', 'inventory_number', 'lastUpdate', 'location', 'blackResidue',
                                               'cyanResidue', 'yellowResidue', 'magentaResidue', 'blackDrumResidue',
                                               'dataFile', 'cyanDrumResidue', 'yellowDrumResidue', 'magentaDrumResidue',
                                               'wasteBoxStatus', 'ip', 'pk')
    OldModels = XeroxColorA3Old.objects.values('name', 'inventory_number', 'lastUpdate', 'location', 'blackResidue',
                                               'cyanResidue', 'yellowResidue', 'magentaResidue', 'blackDrumResidue',
                                               'dataFile', 'wasteBoxStatus', 'ip', 'pk')
    counters = get_counters("xer")
    XeroxPrinters = list(chain(OldModels, NewModels))
    old_printer = [p['name'] for p in OldModels]
    title = "Xerox Color A3 printers"
    return render(request, 'short_printer_report.html',
                  {'printers': XeroxPrinters, 'counters': counters, 'old_printer': old_printer, 'title': title})


def show_XeroxMonoA3(request):
    XeroxPrinters = XeroxMonoA3.objects.values('name', 'inventory_number', 'lastUpdate', 'dataFile', 'location', 'ip',
                                               'blackResidue', 'blackDrumResidue', 'wasteBoxStatus', 'pk')
    counters = get_counters("xer")
    title = "Xerox Mono A3 printers"
    return render(request, 'short_printer_report.html', {'printers': XeroxPrinters, 'counters': counters, 'title': title})


def show_XeroxMonoA4(request):
    XeroxPrinters = XeroxMonoA4.objects.values('name', 'inventory_number', 'ip', 'lastUpdate', 'dataFile',
                                               'location', 'blackResidue', 'pk')
    counters = get_counters("xer")
    title = "Xerox Mono A4 printers"
    return render(request, 'short_printer_report.html', {'printers': XeroxPrinters, 'counters': counters, 'title': title})


def show_XeroxColorA4(request):
    XeroxPrinters = XeroxColorA4.objects.values('name', 'inventory_number', 'lastUpdate', 'location', 'blackResidue',
                                                'cyanResidue', 'yellowResidue', 'magentaResidue', 'dataFile', 'ip', 'pk')
    counters = get_counters("xer")
    title = "Xerox Color A4 printers"
    return render(request, 'short_printer_report.html', {'printers': XeroxPrinters, 'counters': counters, 'title': title})


def show_OkiMonoA4(request):
    OkiPrinters = OkiMonoA4.objects.values('name', 'inventory_number', 'lastUpdate', 'dataFile', 'location',
                                           'blackResidue', 'blackDrumResidue', 'ip', 'pk')
    counters = get_counters("oki")
    title = "OKI Mono printers"
    return render(request, 'short_printer_report.html', {'printers': OkiPrinters, 'counters': counters, 'title': title})

def show_Total(request):
    HPprinters = HPColorA3.objects.values()
    XerNewModels = XeroxColorA3New.objects.values()
    XerOldModels = XeroxColorA3Old.objects.values()
    XerA3Mono = XeroxMonoA3.objects.values()
    XerA4Mono = XeroxMonoA4.objects.values()
    XerA4Color = XeroxColorA4.objects.values()
    OkiPrinters = OkiMonoA4.objects.values()
    today = datetime.today()  # replace(tzinfo=utc)
    TotalPrinters = list(chain(XerNewModels, XerOldModels, HPprinters, XerA4Color, XerA3Mono, XerA4Mono, OkiPrinters))
    return render(request, 'totalprinters.html', {'printers': TotalPrinters, 'today': today})

def show_PrintedPagesStatistic(request):
    """ Shows top 10 loaded printers in this day, week, month, year """
    thisDay = Printer.objects.order_by('-printedToday').values('name', 'inventory_number', 'location',
                                                               'ip', 'printedToday', 'lastUpdate')[:10]
    thisWeek = Printer.objects.order_by('-printedThisWeek').values('name', 'inventory_number', 'location',
                                                                   'ip', 'printedThisWeek', 'lastUpdate')[:10]
    thisMonth = Printer.objects.order_by('-printedThisMonth').values('name', 'inventory_number', 'location',
                                                                     'ip', 'printedThisMonth', 'lastUpdate')[:10]
    thisYear = Printer.objects.order_by('-printedThisYear').values('name', 'inventory_number', 'location',
                                                                   'ip', 'printedThisYear', 'lastUpdate')[:10]
    return render(request, 'printedpagesstatistic.html', {'thisDay': thisDay, 'thisWeek': thisWeek,
                                                          'thisMonth': thisMonth, 'thisYear': thisYear})

def show_AllChanges(request):
    """ Shows all cartridge installations """
    totalChanges = ChangesCounter.objects.order_by('-date')
    return render(request, 'total_changes.html', {'totalChanges': totalChanges})

def show_DetailedStatus(request, primary_key):
    """This function is receiving primary key of an object and shows in detailed status """
    for model in models:
        try:
            printer = model.objects.filter(pk=primary_key).values()[0]
            counters = get_counters(printer.get("dataFile"))
        except IndexError:
            pass
    printers = list()
    printers.append(printer.copy())  # This action is made to satisfy the loop in template
    return render(request, "detailed_printer_info.html", {"printers": printers, "counters": counters})

def show_menu(request):
    return render(request, 'index.html')


def show_home(request):
    return render(request, 'home.html')


def cartridge_form_input(request):
    """
    This function is used to show form for input sent or received cartridge amount.
    """
    success_msg = ''
    sent_received_statistic = EmptyCartridgeInventory.count_difference()
    if request.method == 'POST':
        diction = dict()
        form = EmptyCartridgeInventoryForm(request.POST, request.FILES)
        if form.is_valid():
            diction['cartridge'] = form.cleaned_data['cartridge']
            diction['amount'] = form.cleaned_data['amount']
            diction['direction'] = form.cleaned_data['direction']
            EmptyCartridgeInventory.dict_validator(diction)
            form = EmptyCartridgeInventoryForm()
            success_msg = "Success"
        else:
            success_msg = "Form is not Valid"
    else:
        form = EmptyCartridgeInventoryForm()
    return render(request, 'new_input.html', {'form': form, 'success_msg': success_msg,
                                              'sent_received_statistic': sent_received_statistic})

