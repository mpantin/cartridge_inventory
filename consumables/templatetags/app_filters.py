from django import template
from django.utils.timezone import utc
from datetime import datetime, timedelta

register = template.Library()

colors = {"red": "#FF0000", "green": "#008000"}

def applyFont(text, color="red"):
    ''' This function applies font to the text. By default - red '''
    font = ["<b><font color=", ">", "</font></b>"]
    text = font[0] + colors.get(color) + font[1] + text + font[2]
    return text


@register.filter(name='update_marker')
def update_marker(value):
    delta = datetime.today().replace(tzinfo=utc) - value
    if (delta.days == 1) or (delta.days > 1):
        return applyFont(value.strftime('%b. %d, %Y %H:%M'))
    else:
        return value


@register.filter(name='date_parser')
def date_parser_red_marker(value):
    delta = datetime.today().replace(tzinfo=utc) - value
    if delta.days > 2:
        return True
    else: 
        return False


@register.filter(name='residue_value_marker')
def residue_value_marker(value):
    if value:
        value = int(value)
        if value >= 0 and value < 5:
            return applyFont(str(value)+"%")
        elif value == -3:
            return applyFont("OK", "green")
        else:
            return str(value) + "%"
    elif value == 0:
        return applyFont(str(value)+"%")
    else:
        return "-"


@register.filter(name='residue_key_perser')
def residue_key_parser(value):
    value = value.replace("Residue", "")
    if "Drum" in value:
        value = value.replace("Drum", " Drum").title()
    elif "transfer" or "fuser" in value:
        value = value.title() + " Kit"
    elif "Update" in value:
        value.replace("Update", " Update").title()
    else:
        value.title()
    return value


@register.filter(name='name_changer')
def name_changer(value):
    if 'xer' in value:
        value = value.replace("xer", "Xerox ").title()
    elif 'hp' in value:
        value = value.replace("hp", "HP ").title()
    return value


@register.filter(name='waste_status')
def waste_status_parser(value):
    if value:
        if any(s in str(value) for s in ['-1', '-3', '100', '150000']):
            value = applyFont("OK", "green")
        elif value == -2:
            value = applyFont("No Waste Box")
        elif value == 5:
            value = applyFont("Reorder")
        else:
            value = applyFont("Full")
        return value
    elif value == 0:
        return applyFont("Needs checking")
    else:
        return "-"


@register.filter(name='dataFile_to_name')
def dataFile_to_name(value):
    value = value.split('_')
    result = str()
    for v in value[:-1]:
        result = result + " " + name_changer(v)
    return result

@register.filter(name='mark_red')
def mark_red(value):
    value = '<b>' + applyFont(value) + '</b>'
    return value

@register.filter(name='status_changer')
def status_changer(value):
    delta = datetime.today().replace(tzinfo=utc) - value
    if delta.days < 1:
        return "OK"