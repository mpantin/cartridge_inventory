from django.contrib import admin
from models import Files, Kind, Color, Consumable, Printer, HPColorA3, ChangesCounter, XeroxMonoA3, XeroxColorA3New, XeroxColorA3Old, OkiMonoA4, XeroxMonoA4, XeroxColorA4, GraphBase, EmptyCartridgeInventory, Cartridge
# Register your models here.

class GraphBaseAdmin(admin.ModelAdmin):
    list_display = ('date', 'dataFile', 'consumable', 'counter')
    list_filter = ['dataFile', 'consumable']
    list_search = ['dataFile', 'consumable']


class ChangesCounterAdmin(admin.ModelAdmin):
    list_display = ('date', 'dataFile', 'consumable', 'counter')
    list_filter = ['dataFile', 'consumable']
    list_search = ['dataFile', 'consumable']

class AllPrintersAdmin(admin.ModelAdmin):
    list_display = ('name', 'inventory_number', 'location', 'ip', 'lastUpdate', 'totalPages')

class EmptyCartridgeInventoryAdmin(admin.ModelAdmin):
    list_display = ('date', 'display_cartridge_fields', 'amount', 'direction')

    def display_cartridge_fields(self, obj):
        return obj.cartridge.model + " " + obj.cartridge.color
    display_cartridge_fields.short_description = 'model'
    display_cartridge_fields.admin_order_field = 'Cartridge__model'


admin.site.register(Files)
admin.site.register(Kind)
admin.site.register(Color)
admin.site.register(Consumable)
admin.site.register(Printer, AllPrintersAdmin)
admin.site.register(HPColorA3, AllPrintersAdmin)
admin.site.register(XeroxMonoA3, AllPrintersAdmin)
admin.site.register(XeroxColorA3New, AllPrintersAdmin)
admin.site.register(XeroxColorA3Old, AllPrintersAdmin)
admin.site.register(OkiMonoA4, AllPrintersAdmin)
admin.site.register(ChangesCounter, ChangesCounterAdmin)
admin.site.register(XeroxMonoA4, AllPrintersAdmin)
admin.site.register(XeroxColorA4, AllPrintersAdmin)
admin.site.register(GraphBase, GraphBaseAdmin)
admin.site.register(Cartridge)
admin.site.register(EmptyCartridgeInventory, EmptyCartridgeInventoryAdmin)
