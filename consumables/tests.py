from django.test import TestCase
from models import *
from django.utils import timezone
import os, datetime


path = '/var/projects/cartridge_inventory/data/test_data/printer_files/'


class GlobalFunctionsTestCase(TestCase):
    def test_mono_cartridge_residue(self):
        test = createCartridgeResidueName()
        self.assertEqual(test[0], "blackResidue")

    def test_color_cartridge_residue(self):
        test = createCartridgeResidueName("full")
        self.assertEqual(test[0], "blackResidue")
        self.assertEqual(test[1], "cyanResidue")
        self.assertEqual(test[2], "yellowResidue")
        self.assertEqual(test[3], "magentaResidue")

    def test_mono_drum_residue(self):
        test = createDrumResidueName()
        self.assertEqual(test[0], "blackDrumResidue")

    def test_color_drum_residue(self):
        test = createDrumResidueName("full")
        self.assertEqual(test[0], "blackDrumResidue")
        self.assertEqual(test[1], "cyanDrumResidue")
        self.assertEqual(test[2], "yellowDrumResidue")
        self.assertEqual(test[3], "magentaDrumResidue")


class test_FilesClassTestCase(TestCase):
    def setUp(self):
        OkiMonoA4.objects.create(name="Test print", dataFile="exampleDataFile", location="here",
                                 blackResidue=30, blackDrumResidue=40)

    def test_check_xerox5230(self):
        files = Files()
        files.lookUp(path)
        Oki430 = OkiMonoA4.objects.get(pk=1)
        self.assertEqual(Xerox5230.Location, "site areaB")
