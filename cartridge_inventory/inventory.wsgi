import os
import sys
sys.path = ['/var/projects/cartridge_inventory'] + sys.path
os.environ['DJANGO_SETTINGS_MODULE'] = 'cartridge_inventory.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()