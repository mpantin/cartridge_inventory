from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from cartridge_inventory.views import check_changes
from consumables.views import show_HPColor, show_XeroxColorA3, show_home, show_XeroxMonoA3, show_OkiMonoA4, show_XeroxMonoA4, show_XeroxColorA4, show_Total, show_PrintedPagesStatistic, show_AllChanges, show_DetailedStatus, cartridge_form_input

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'cartridge_inventory.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),
                       url(r'^$', show_home, name='home'),
                       url(r'^check_changes/', check_changes),
                       url(r'^hpcolor/', show_HPColor),
                       url(r'^xeroxcolora3/', show_XeroxColorA3),
                       url(r'^xeroxmonoa3/', show_XeroxMonoA3),
                       url(r'^xeroxcolora4/', show_XeroxColorA4),
                       url(r'^xeroxmonoa4/', show_XeroxMonoA4),
                       url(r'^okimonoa4/', show_OkiMonoA4),
                       url(r'^total/', show_Total),
                       url(r'^pagesstatistic/', show_PrintedPagesStatistic),
                       url(r'^detailed_status/(?P<primary_key>[0-9]+)/$', show_DetailedStatus),
                       url(r'^home/', show_home),
                       url(r'^total_changes/', show_AllChanges),
                       # url(r'^input_man/', test__manual_form),
                       url(r'^transfer_input/', cartridge_form_input),
                       url(r'^admin/', include(admin.site.urls)),
                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

